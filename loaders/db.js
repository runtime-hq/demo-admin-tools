const Knex = require("knex");
const knexConfig = require("../config/knexfile");

module.exports = function loadDb() {
  return Knex(knexConfig);
};
