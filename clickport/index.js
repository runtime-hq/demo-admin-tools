/**
 * This module is a minimal proof-of-concept for the ClickPort NPM package.
 */

const axios = require("axios");

const DEFAULT_HOST = "clickport.io";
const DEFAULT_BASE_PATH = "/api/";

/**
 * Respond to the Slack command.
 * When ClickPort executes a script, it sets the `RESPONSE_TOKEN` environment variable.
 * We then POST to the ClickPort server with this token. If valid, the response is forwarded to Slack.
 * @param {*} payload the data to display in Slack.
 * @returns
 */
async function postResponse(payload) {
  if (!process.env.RESPONSE_TOKEN) {
    throw new Error("RESPONSE_TOKEN environment variable not found.");
  }

  return axios.post(
    `https://${DEFAULT_HOST}${DEFAULT_BASE_PATH}response`,
    payload,
    {
      headers: {
        Authorization: `Bearer ${process.env.RESPONSE_TOKEN}`,
      },
    }
  );
}

function ClickPort() {
  return {
    postResponse,
  };
}

module.exports = ClickPort;
