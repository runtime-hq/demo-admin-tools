const process = require("process");
const { program } = require("commander");
const clickport = require("../clickport")(); // NOTE: clickport will be an installable NPM package.
const loadDb = require("../loaders/db");

const db = loadDb();

// ClickPort will pass the `team_id` argument to the script as we defined in the `.clickport.yml` file.
program.option("-t, --team_id <id>", "team id");

async function main() {
  // 1. Parse the arguments. Here, we get the team ID.
  program.parse(process.argv);
  const { team_id } = program.opts();

  // 2. Do something. Here, we query our database to get some data about the team.
  const team = await db("team").select().where("id", team_id).first();

  // 3. Respond to the command.
  // `postResponse` responds to the ClickPort server, which will forward the response to Slack.
  await clickport.postResponse({
    text: `Team: ${team.name}`,
  });
}

main()
  .catch((e) => console.error(e))
  .finally(() => process.exit(1));
