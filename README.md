# demo-admin-tools

This repository is a demonstration of **ClickPort**: The Slack app for your internal tools.

Your organisation probably has a repository like this one (`demo-admin-tools`). The repository contains scripts that perform operational tasks, like:

- Retrieving customer data.
- Setting feature flags for a customer.
- Refunding a customer.
- Deleting an account.

With ClickPort, you can make these tools instantly accessible within Slack. Your entire team can access the tools they need, when they need them.

## Tl;dr

Add a YAML file, deploy your tools repository with the `clickport-agent`, and you've got yourself a fully-featured Slack app for your internal tools 😁.

## About this repository

The `scripts/` directory contains our scripts. Currently, it contains one script: [`./scripts/get_team.js`](scripts/get_team.js), but it could contain many scripts written in a variety of programming languages.

[`./scripts/get_team.js`](scripts/get_team.js) retrieves data about a team.
It takes a single argument - `team_id` - and queries the database for the `team` record matching the given `team_id`.

## How it works

Let's turn [`./scripts/get_team.js`](scripts/get_team.js) into a Slack app.

1. Define the [`.clickport.yml`](.clickport.yml) file.

   The `.clickport.yml` file defines the scripts we want to expose to Slack. Let's define the `get_team` script.

   ```yml
   get_team:
     name: "Get team"
     description: "Retrieve basic data about a team."
     parameters:
       - id: "team-id"
         name: "Team ID"
         description: "The team ID of the team."
         placeholder: "e.g. 12345"
         flag: "--team_id" # this flag will be passed to our execution command for the script.
     script:
       - "node ./scripts/get_team.js" # this can be any script in any programming language.
   ```

1. Post the script output to ClickPort.

   Scripts typically log output to the console. Because our end-user is in Slack, we need to add some
   simple code to send our output to Slack.

   ClickPort sets the `RESPONSE_TOKEN` environment variable when it executes a script. This is a short-lived JWT that will authenticate and indentify our request.

   ```js
   // < script code here >

   return axios.post(
     `https://clickport.io/api/response`,
     { text: `The team name is ${team.name}` },
     {
       headers: {
         Authorization: `Bearer ${process.env.RESPONSE_TOKEN}`,
       },
     }
   );
   ```

   Alternatively, we can use the `clickport` package to achieve this:

   ```js
   const clickport = require("clickport")();

   // < script code here >

   await clickport.postResponse({
     text: `Team: ${team.name}`,
   });
   ```

1. Download the `clickport-agent`.

   The [`clickport-agent`](https://gitlab.com/clickport/clickport-agent) starts a basic web server to handle incoming requests from Slack.
   It's [open source](https://gitlab.com/clickport/clickport-agent), so you're welcome to
   inspect the code to ensure it aligns with your organization's security posture.

1. Deploy the agent.

   The final step is to deploy the `clickport-agent`.
   In practice, this means:

   1. Cloning this repository to an AWS EC2 instance, Heroku dyno, DO droplet etc. (whereever you like!)
   1. Configuring the instance with the appropriate environment variables.
   1. Starting the agent process (`./clickport-agent`).
